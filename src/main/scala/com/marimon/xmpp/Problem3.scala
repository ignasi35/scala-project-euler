package com.marimon.xmpp
import scala.collection.Set
import scala.collection.immutable.HashSet

class Problem3 {

  // adapted from http://www.vogella.com/articles/JavaAlgorithmsPrimeFactorization/article.html
  def factorizeMax(input: Long): Long = {
    var n: Long = input;
    var factor: Long = 0;
    var i = 2L;
    while ((i <= n / i)) {
      while (n % i == 0) {
        n /= i;
      }
      i += 1;
    }
    if (n > 1) {
      factor = n;
    }
    return factor;
  }
}