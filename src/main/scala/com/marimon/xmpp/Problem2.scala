package com.marimon.xmpp

class Problem2 {

  var status: Int = 0

  var previous: Int = 0

  def iterator(): Iterator[Int] = {
    return Iterator.continually[Int]({
      val result: Int = if (status == 0 && previous == 0) 1 else status + previous;
      previous = status;
      status = result;
      result
    });
  }

  var itr = iterator()

  var count: Int = 0;

  private def skipIterator(): Iterator[Int] = {
    return Iterator.continually[Int]({
      if (count % 3 == 2) {
        itr.next
        count += 1
      }
      count += 1;
      itr.next
    });
  }

  def solve(): Int = {
    skipIterator().takeWhile(_ < 4000000).foldLeft(0)((r, c) => r + c)
  }

}

