package com.marimon.xmpp

object Problem1 {

  def find(limit: Int): IndexedSeq[Int] = {
    for (
      i <- 1 to (limit - 1) if i % 3 == 0 || i % 5 == 0
    ) yield i
  }

  def sum(values: Seq[Int]): Int = {
    values.foldLeft(0)((b, a) => b + a)
  }

  def solve(input: Int): Int = {
    {
      for (
        i <- 1 to (input - 1) if i % 3 == 0 || i % 5 == 0
      ) yield i
    }.foldLeft(0)((b, a) => b + a)

  }

}