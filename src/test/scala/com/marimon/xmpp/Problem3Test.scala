package com.marimon.xmpp
import org.scalatest.FlatSpec
import org.scalatest.matchers.ShouldMatchers

class Problem3Test extends FlatSpec with ShouldMatchers {

  "A Problem3" should "factorize 15 into 5 and 3 and return 5" in {
    new Problem3().factorizeMax(15) should be(5)
  }

  it should "be solved for input is 600851475143" in {
    new Problem3().factorizeMax(600851475143L) should be(6857)
    new Problem3().factorizeMax(87625999) should be(1471)
    new Problem3().factorizeMax(59569) should be(839)
    new Problem3().factorizeMax(71) should be(71)

  }
}