package com.marimon.xmpp
import org.scalatest.FlatSpec
import org.scalatest.matchers.ShouldMatchers
import org.junit.runner.RunWith
import org.scalatest.junit.JUnitRunner
import scala.collection.immutable.IndexedSeq

@RunWith(classOf[JUnitRunner])
class Problem1Test extends FlatSpec with ShouldMatchers {
  //If we list all the natural numbers below 10 that are multiples of 
  //3 or 5, we get 3, 5, 6 and 9. The sum of these multiples is 23.
  //Find the sum of all the multiples of 3 or 5 below 1000.

  "The problem 1" should "find all multiples of 3 or 5 under 10" in {
    Problem1.find(10) should be(IndexedSeq(3, 5, 6, 9))
  }

  it should "sum the provided numbers" in {
    Problem1.sum(IndexedSeq(1, 2, 3, 4)) should be(10)
  }

  it should "solve the problem" in {
    Problem1.solve(1000) should be(233168)
  }
}