name := "scala-project-euler"

version := "0.0.1"

scalaVersion := "2.9.1" 

libraryDependencies += "org.scalatest" %% "scalatest" % "1.7.2" % "test"

libraryDependencies += "junit" % "junit" % "4.8.2" % "test"

